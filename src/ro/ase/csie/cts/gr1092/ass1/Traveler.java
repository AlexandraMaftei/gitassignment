package ro.ase.csie.cts.gr1092.ass1;

public class Traveler implements Travel {
	public String username;
	public String countryOfResidence;
	public boolean hasPassport;
	public double budget;
	
	public Traveler(String username, String countryOfResidence, boolean hasPassport, double budget) {
		super();
		this.username = username;
		this.countryOfResidence = countryOfResidence;
		this.hasPassport = hasPassport;
		this.budget = budget;
	}

	@Override
	public void checkTravelingDocuments(boolean hasPassport) {
		// TODO Auto-generated method stub
		if (hasPassport == false) {
			System.out.println("You cannot travel to another continent without a passport");
		}
		else
		{
			System.out.println("You should check the visa requirements for the country you are traveling to.");
		}
	}
}
