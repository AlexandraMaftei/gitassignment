package ro.ase.csie.cts.gr1092.ass1;

public interface Travel {
	public void checkTravelingDocuments(boolean hasPassport);
}
